/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lab5part2;

/**
 *
 * @author harpreet singh
 */
public class Student {

     private String name;
     private String rollNo;
     private String id;
    
     public String getRollNo(){
         return rollNo;
    }
    
     public void setRollNo(String rollNo){
         this.rollNo=rollNo;
     }
     
     public String getName(){
         return name;
     }
     
     public void setName(String name){
         this.name=name;
     }
     public String getId(){
         return id;
     }
     
     public void setId(String id){
         this.id=id;
     }
}
