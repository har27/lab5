/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab5part2;

/**
 *
 * @author harpreet singh
 */
public class MVCPatternDemo {
    public static void main(String[] args){
        
        Student model = retriveStudentFromDatabase();
        
        StudentView view = new StudentView();
        
        StudentController controller = new StudentController(model, view);
        
        controller.updateView();
        
        controller.setStudentName("Har");
        
        controller.updateView();
    }
        
    private static Student retriveStudentFromDatabase(){
        Student student = new Student();
        student.setName("Har");
        student.setRollNo("1");
        student.setId("99");
        return student;
    }
    
}
